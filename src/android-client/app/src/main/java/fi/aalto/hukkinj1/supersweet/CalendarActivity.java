package fi.aalto.hukkinj1.supersweet;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.alamkanak.weekview.WeekViewEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class CalendarActivity extends ActionBarActivity {

    private String mUsername;
    private String mCalendarName;
    private List<Event> mCalendarEvents;

    private SuperSweetService mSuperSweetApi;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    private NonSwipeableViewPager mViewPager;

    public Syncer mSyncer;

    SuperSweetService ssApi = ServiceGenerator.createService(SuperSweetService.class);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        mSyncer = new Syncer(getApplicationContext());

        mUsername = getIntent().getStringExtra(FetchCalendarActivity.EXTRA_USERNAME);
        mCalendarName = getIntent().getStringExtra(FetchCalendarActivity.EXTRA_CALENDAR_NAME);

        mSuperSweetApi = ServiceGenerator.createService(SuperSweetService.class);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (NonSwipeableViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        updateEventsFromAPI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /* Go to day view and set visible day to the one given as a parameter */
    public void goToDayView(java.util.Calendar day) {
        mViewPager.setCurrentItem(1);
        getDayViewFragment().goToDay(day);
    }

    /**
     * Override "hardware" back button listener so that when the user is in day view, they go back
     * to month view and the activity does not close.
     */
    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 1) {
            mViewPager.setCurrentItem(0);
        }
        else {
            super.onBackPressed();
        }
    }

    public void askEventTitle() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Give event title");

// Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Event event = mSyncer.syncEventFromAndroidCalendar(input.getText().toString());
                if (event == null) {
                    Log.d(getClass().getName(), "Could not find event.");
                }
                else {
                    Log.d(getClass().getName(), "Found event " + event.title + event.start.toString() + event.end.toString());
                    Call<Event.CommunicationEvent> call = ssApi.createEvent(mUsername, mCalendarName, event.getSendable());
                    call.enqueue(new Callback<Event.CommunicationEvent>() {
                        @Override
                        public void onResponse(Response<Event.CommunicationEvent> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                updateEventsFromAPI();
                            } else {
                                Log.d(getClass().getName(), "Couldn't send event. HTTP status: " + response.errorBody());
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            Log.d(getClass().getName(), t.getMessage());
                        }
                    });
                }
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                // ProjectsActivity is my 'home' activity
                onBackPressed();
                return true;
            case R.id.event_from_phone:
                askEventTitle();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void updateEvents(List<Event> newEvents) {
        mCalendarEvents = newEvents;
        Log.d(getClass().getName(), "Events stored to CalendarActivity.mCalendarEvents");

        getMonthViewFragment().update(mCalendarEvents);
        getDayViewFragment().update(mCalendarEvents);
    }

    private Event findEventByWeekViewEvent(WeekViewEvent wvEvent) {
        List<Long> hashCodes = new ArrayList<Long>();
        for (Event event : mCalendarEvents) {
            hashCodes.add((Long)(long)event.icaluid.hashCode());
        }
        return mCalendarEvents.get(hashCodes.indexOf(wvEvent.getId()));
    }

    public void saveEventToLocalCalendar(WeekViewEvent wvEvent) {
        Event event = findEventByWeekViewEvent(wvEvent);
        mSyncer.syncEventToAndroidCalendar(event);
    }

    public void createEvent(java.util.Calendar time) {
        Log.d(getClass().getName(), "Creating event " + mUsername + " " + mCalendarName + " " + time.getTime().toString());
        DayViewFragment dayView = getDayViewFragment();
        String newTitle = ((EditText)dayView.mpopup.getContentView().findViewById(R.id.event_title)).getText().toString();
        String newStartTime = ((EditText)dayView.mpopup.getContentView().findViewById(R.id.event_start_time)).getText().toString();
        String newEndTime = ((EditText)dayView.mpopup.getContentView().findViewById(R.id.event_end_time)).getText().toString();
        Date start;
        Date end;
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
            start = df.parse(newStartTime);
            end = df.parse(newEndTime);
        }
        catch(Exception ex) {
            Toast.makeText(getApplicationContext(),"Invalid time!" + ex.toString(), Toast.LENGTH_LONG).show();
            Log.e(getClass().getName(), "Invalid time! " + ex.toString());
            return;
        }
        Event event = new Event(start, end, newTitle);
        Log.d(getClass().getName(), "New event: " + event.title + " " + event.start.toString() + " " + event.end.toString());
        Call<Event.CommunicationEvent> call = ssApi.createEvent(mUsername, mCalendarName, event.getSendable());
        call.enqueue(new Callback<Event.CommunicationEvent>() {
            @Override
            public void onResponse(Response<Event.CommunicationEvent> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    updateEventsFromAPI();
                } else {
                    Toast.makeText(getApplicationContext(), "Couldn't create event. HTTP status: " + response.message(), Toast.LENGTH_LONG).show();
                    Log.d(getClass().getName(), "Couldn't create event. HTTP status: " + response.code() + " " + response.message());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(getClass().getName(), t.getMessage());
            }
        });
    }

    public void updateEvent(WeekViewEvent event) {
        Event calendarEvent = findEventByWeekViewEvent(event);
        Log.d(getClass().getName(), "Updating event " + mUsername + " " + mCalendarName + " " + calendarEvent.title + " " + calendarEvent._id);
        DayViewFragment dayView = getDayViewFragment();
        String newTitle = ((EditText)dayView.mpopup.getContentView().findViewById(R.id.event_title)).getText().toString();
        String newStartTime = ((EditText)dayView.mpopup.getContentView().findViewById(R.id.event_start_time)).getText().toString();
        String newEndTime = ((EditText)dayView.mpopup.getContentView().findViewById(R.id.event_end_time)).getText().toString();
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
            calendarEvent.start = df.parse(newStartTime);
            calendarEvent.end = df.parse(newEndTime);
        }
        catch(Exception ex) {
            Toast.makeText(getApplicationContext(),"Invalid time!" + ex.toString(), Toast.LENGTH_LONG).show();
            Log.e(getClass().getName(), "Invalid time! " + ex.toString());
            return;
        }
        calendarEvent.title = newTitle;
        Log.d(getClass().getName(), "New data: " + calendarEvent.title + " " + calendarEvent.start.toString() + " " + calendarEvent.end.toString());
        Call<Event.CommunicationEvent> call = ssApi.updateEvent(mUsername, mCalendarName, calendarEvent._id, calendarEvent.getSendable());
        call.enqueue(new Callback<Event.CommunicationEvent>() {
            @Override
            public void onResponse(Response<Event.CommunicationEvent> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    updateEventsFromAPI();
                } else {
                    Toast.makeText(getApplicationContext(), "Couldn't update event. HTTP status: " + response.message(), Toast.LENGTH_LONG).show();
                    Log.d(getClass().getName(), "Couldn't update event. HTTP status: " + response.code() + " " + response.message());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(getClass().getName(), t.getMessage());
            }
        });
    }

    public void deleteEvent(WeekViewEvent event) {
        Event calendarEvent = findEventByWeekViewEvent(event);
        Log.d(getClass().getName(), "Deleting event " + mUsername + " " + mCalendarName + " " + calendarEvent.title + " " + calendarEvent._id);
        Call<Event> call = ssApi.deleteEvent(mUsername, mCalendarName, calendarEvent._id);
        call.enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Response<Event> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    updateEventsFromAPI();
                } else {
                    Toast.makeText(getApplicationContext(), "Couldn't delete event. HTTP status: " + response.message(), Toast.LENGTH_LONG).show();
                    Log.d(getClass().getName(), "Couldn't delete event. HTTP status: " + response.code() + " " + response.message());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d(getClass().getName(), t.getMessage());
            }
        });
    }

    /**
     * If succesful, overwrites events stored in mCalendarEvents with events that are in database
     */
    public void updateEventsFromAPI() {
        Call<List<Event>> call = mSuperSweetApi.getCalendarEvents(mUsername, mCalendarName);
        call.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Response<List<Event>> response, Retrofit retrofit) {
                if (response.isSuccess()) {

                    Log.d(getClass().getName(), "Events received from server: " + response.body().toString());
                    updateEvents(response.body());

                } else {
                    // error response, no access to resource?
                    Toast.makeText(getApplicationContext(), "Couldn't fetch events. HTTP status: " + response.code(), Toast.LENGTH_LONG).show();
                    Log.d(getClass().getName(), "Couldn't fetch events. HTTP status: " + response.code());
                }
            }

            @Override
            public void onFailure(Throwable t) {
                // something went completely south (like no internet connection)
                Log.d(getClass().getName(), "Failed to get events.");
            }
        });
    }

    private MonthViewFragment getMonthViewFragment() {
        return (MonthViewFragment) ((SectionsPagerAdapter)mViewPager.getAdapter()).getFragment(0);
    }

    private DayViewFragment getDayViewFragment() {
        return (DayViewFragment) ((SectionsPagerAdapter)mViewPager.getAdapter()).getFragment(1);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private Map<Integer, CalendarFragment> mPageReferenceMap = new HashMap<Integer, CalendarFragment>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            Fragment item = null;
            if (position == 0) {
                item = MonthViewFragment.newInstance();
                mPageReferenceMap.put(position, (CalendarFragment)item);
            }
            else if (position == 1) {
                item = DayViewFragment.newInstance();
                mPageReferenceMap.put(position, (CalendarFragment)item);
            }
            return item;
        }

        @Override
        public void destroyItem (ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            mPageReferenceMap.remove(position);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
            }
            return null;
        }

        public CalendarFragment getFragment(int key) {
            return mPageReferenceMap.get(key);
        }
    }
}
