package fi.aalto.hukkinj1.supersweet;

import android.support.v4.app.Fragment;

import java.util.List;


abstract public class CalendarFragment extends Fragment {
    abstract public void update(List<Event> events);
}
