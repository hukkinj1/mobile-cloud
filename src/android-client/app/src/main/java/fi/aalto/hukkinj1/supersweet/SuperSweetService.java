package fi.aalto.hukkinj1.supersweet;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;

/**
 * Created by hukkinj1 on 11/13/15.
 */
public interface SuperSweetService {

    @GET("/users/{username}/calendars/{calendar_name}/events")
    Call<List<Event>> getCalendarEvents(
            @Path("username") String username,
            @Path("calendar_name") String calendarName);

    @GET("/users/{username}/calendars/{calendar_name}")
    Call<Calendar> getCalendar(
            @Path("username") String username,
            @Path("calendar_name") String calendarName);

    @POST("/users/{username}/calendars/{calendar_name}/events/")
    Call<Event.CommunicationEvent> createEvent(
            @Path("username") String username,
            @Path("calendar_name") String calendarName,
            @Body Event.CommunicationEvent event);

    @DELETE("users/{username}/calendars/{calendar_name}/events/{event_id}")
    Call<Event> deleteEvent(
            @Path("username") String username,
            @Path("calendar_name") String calendarName,
            @Path("event_id") String eventId);

    @POST("users/{username}/calendars/{calendar_name}/events/{event_id}")
    Call<Event.CommunicationEvent> updateEvent(
            @Path("username") String username,
            @Path("calendar_name") String calendarName,
            @Path("event_id") String eventId,
            @Body Event.CommunicationEvent event);
}
