package fi.aalto.hukkinj1.supersweet;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.util.Log;

import java.util.Date;
import java.util.TimeZone;

/**
 * Created by jay on 19.11.2015.
 */


public class Syncer {

    private ContentResolver mContentResolver;
    public static final String[] CALENDAR_FIELDS = {
            CalendarContract.Calendars._ID
    };

    public static final String[] EVENT_FIELDS = new String[] {
            CalendarContract.Events.DTSTART,
            CalendarContract.Events.DTEND,
            CalendarContract.Events.TITLE,
            CalendarContract.Events.CALENDAR_ID
    };

    public Syncer(Context context) {
      mContentResolver = context.getContentResolver();
    };

    private String getSomeCalendarId() {
        Cursor cursor = mContentResolver.query(Uri.parse("content://com.android.calendar/calendars"),
                CALENDAR_FIELDS,
                null,
                null,
                null);
        String calendarId = null;
        try {
            if (cursor.getCount() > 0) {
                cursor.moveToNext();
                Log.d(getClass().getName(), "Found " + cursor.getString(0) + " calendars.");
                calendarId = cursor.getString(0);
            }
        } catch (NullPointerException ex) {
            return null;
        }
        return calendarId;
    }

    public void syncEventToAndroidCalendar(Event event) {
        String calendarId = getSomeCalendarId();
        if (calendarId == null) {
            Log.d(getClass().getName(), "Could not find any local calendar.");
            return;
        }
        Uri.Builder builder = Uri.parse("content://com.android.calendar/events/").buildUpon();

        ContentValues values = new ContentValues();
        values.put(CalendarContract.Events.DTSTART, event.start.getTime());
        values.put(CalendarContract.Events.DTEND, event.end.getTime());
        values.put(CalendarContract.Events.TITLE, event.title);
        values.put(CalendarContract.Events.CALENDAR_ID, calendarId);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, "UTC");
        try {
            Uri uri = mContentResolver.insert(CalendarContract.Events.CONTENT_URI, values);
        }
        catch(SecurityException err) {
            Log.d(getClass().getName(), "Not authorized to create event to calendar.");
        }
    }

    public Event syncEventFromAndroidCalendar(String eventTitle) {
        String calendarId = getSomeCalendarId();
        if (calendarId == null) {
            Log.d(getClass().getName(), "Could not find any local calendar.");
            return null;
        }
        Uri.Builder builder = Uri.parse("content://com.android.calendar/events/").buildUpon();



        String selection = "((" + CalendarContract.Events.TITLE + " = ?) AND (" +
                CalendarContract.Events.CALENDAR_ID + " = ?))";
        String[] selectionArgs = new String[] {eventTitle, calendarId};
        Cursor event =  mContentResolver.query(builder.build(),
                EVENT_FIELDS,
                selection,
                selectionArgs,
                null);
        try {
            if (event.getCount() == 0) {
                Log.d(getClass().getName(), "Could not find event " + eventTitle);
                event.close();
                return null;
            }
        }
        catch(NullPointerException ex) {
            return null;
        }
        event.moveToNext();
        String title = null;
        long begin = 0;
        long end = 0;

        begin = event.getLong(0);
        end = event.getLong(1);
        title = event.getString(2);
        event.close();
        return new Event(new Date(begin), new Date(end), title);
    }
}