package fi.aalto.hukkinj1.supersweet;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jay on 27.11.2015.
 */
public class gsonUTCdateAdapter implements JsonSerializer<Date>,JsonDeserializer<Date> {

    private final DateFormat deserializeDateFormat;
    private final DateFormat serializeDateFormat;

    public gsonUTCdateAdapter() {
        deserializeDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        serializeDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());;
    }

    @Override
    public synchronized JsonElement serialize(Date date, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonPrimitive prim = new JsonPrimitive(serializeDateFormat.format(date));
        Log.d(getClass().getName(), "Serialize" + prim.toString());
        return prim;
    }

    @Override
    public synchronized Date deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        try {
            Date d = deserializeDateFormat.parse(jsonElement.getAsString());
            Log.d(getClass().getName(), "Deserialize: " + d.toString());
            return d;
        } catch (ParseException e) {
            throw new JsonParseException(e);
        }
    }
}
