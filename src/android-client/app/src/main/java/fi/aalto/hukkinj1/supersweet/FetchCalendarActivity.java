package fi.aalto.hukkinj1.supersweet;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import retrofit.Call;


/**
 * A login screen that offers login via email/password.
 */
public class FetchCalendarActivity extends ActionBarActivity {

    // Keys for intent extras
    public final static String EXTRA_USERNAME = "fi.aalto.hukkinj1.supersweet.USERNAME";
    public final static String EXTRA_CALENDAR_NAME = "fi.aalto.hukkinj1.supersweet.CALENDAR_NAME";

    /**
     * Keep track of the fetch task to ensure we can cancel it if requested.
     */
    private CalendarFetchTask mFetchTask = null;

    // UI references.
    private EditText mUsernameView;
    private EditText mCalendarNameView;
    private View mProgressView;
    private View mFetchFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_calendar);
        // Set up the login form.
        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);

        mCalendarNameView = (EditText) findViewById(R.id.calendar_name);

        Button mFetchCalendarButton = (Button) findViewById(R.id.fetch_calendar_button);
        mFetchCalendarButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptFetch();
            }
        });

        mFetchFormView = findViewById(R.id.fetch_form);
        mProgressView = findViewById(R.id.fetch_progress);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptFetch() {
        if (mFetchTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mCalendarNameView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String calendarName = mCalendarNameView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid calendarName
        if (TextUtils.isEmpty(calendarName)) {
            mCalendarNameView.setError(getString(R.string.error_field_required));
            focusView = mCalendarNameView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt calendar fetch and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the calendar fetch attempt.
            showProgress(true);
            mFetchTask = new CalendarFetchTask(username, calendarName);
            mFetchTask.execute((Void) null);
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mFetchFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mFetchFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mFetchFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mFetchFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class CalendarFetchTask extends AsyncTask<Void, Void, CalendarFetchTask.ResultWrapper> {

        private final String mUsername;
        private final String mCalendarName;

        CalendarFetchTask(String username, String calendarName) {
            mUsername = username;
            mCalendarName = calendarName;
        }

        @Override
        protected ResultWrapper doInBackground(Void... params) {
            ResultWrapper result = new ResultWrapper();

            SuperSweetService ssApi = ServiceGenerator.createService(SuperSweetService.class);
            Call<Calendar> call = ssApi.getCalendar(mUsername, mCalendarName);
            Calendar calendar = null;
            try {
                calendar = call.execute().body();
            } catch (IOException e) {
                result.errorMessage = "Unable to connect to server";
                return result;
            }

            // return the calendar here. Get these again in onPostExecute
            result.calendar = calendar;
            return result;
        }

        @Override
        protected void onPostExecute(final ResultWrapper results) {
            mFetchTask = null;
            showProgress(false);

            if (results.errorMessage != null) {
                Toast.makeText(getApplicationContext(), results.errorMessage, Toast.LENGTH_LONG)
                        .show();
            }
            else if (results.calendar != null) {
                Log.d(getClass().getName(), results.calendar.toString());

                Intent intent = new Intent(FetchCalendarActivity.this, CalendarActivity.class);
                intent.putExtra(EXTRA_USERNAME, mUsername);
                intent.putExtra(EXTRA_CALENDAR_NAME, mCalendarName);
                startActivity(intent);

            } else {
                mCalendarNameView.setError(getString(R.string.error_incorrect_calendar));
                mCalendarNameView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mFetchTask = null;
            showProgress(false);
        }

        public class ResultWrapper {
            public String errorMessage;
            public Calendar calendar;
        }
    }
}

