package fi.aalto.hukkinj1.supersweet;

/**
 * Created by hukkinj1 on 11/14/15.
 */
public class Calendar {
    private String title;
    private String ownerId;
    private String _id;

    @Override
    public String toString() {
        return "Calendar{" +
                "title='" + title + '\'' +
                ", ownerId='" + ownerId + '\'' +
                ", _id='" + _id + '\'' +
                '}';
    }
}
