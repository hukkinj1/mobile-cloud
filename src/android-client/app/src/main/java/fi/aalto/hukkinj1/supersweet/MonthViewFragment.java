package fi.aalto.hukkinj1.supersweet;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marcohc.robotocalendar.RobotoCalendarView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MonthViewFragment extends CalendarFragment implements RobotoCalendarView.RobotoCalendarListener {

    private List<Event> mEvents = new ArrayList<Event>();
    private int mCurrentMonthIndex = 0;
    private RobotoCalendarView mRobotoCalendarView;

    public static MonthViewFragment newInstance() {
        MonthViewFragment fragment = new MonthViewFragment();
        //Bundle args = new Bundle();
        //args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_month_view, container, false);

        mRobotoCalendarView = (RobotoCalendarView) rootView.findViewById(R.id.robotoCalendarPicker);

        mRobotoCalendarView.setRobotoCalendarListener(this);

        return rootView;
    }

    @Override
    public void onDateSelected(Date date) {
        mRobotoCalendarView.markDayAsSelectedDay(date);
        ((CalendarActivity)getActivity()).goToDayView(DateHelper.dateToCalendar(date));
    }

    @Override
    public void onRightButtonClick() {
        mCurrentMonthIndex++;
        update();
    }

    @Override
    public void onLeftButtonClick() {
        mCurrentMonthIndex--;
        update();
    }

    public void update() {
        update(mEvents);
    }

    @Override
    public void update(List<Event> events) {
        mEvents = events;
        mRobotoCalendarView.markDayAsCurrentDay(java.util.Calendar.getInstance().getTime());

        java.util.Calendar currentCalendar = java.util.Calendar.getInstance();
        currentCalendar.add(java.util.Calendar.MONTH, mCurrentMonthIndex);
        mRobotoCalendarView.initializeCalendar(currentCalendar);

        // Underline all days that have one or more events happening
        for (Event event: mEvents) {
            for (Date date: DateHelper.getDaysBetweenDates(event.getStart(), event.getEnd())) {
                mRobotoCalendarView.markFirstUnderlineWithStyle(RobotoCalendarView.BLUE_COLOR, date);
            }
        }
    }
}
