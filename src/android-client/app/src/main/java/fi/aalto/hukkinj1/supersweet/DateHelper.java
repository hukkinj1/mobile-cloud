package fi.aalto.hukkinj1.supersweet;

import android.util.Log;

import java.util.*;
import java.util.Calendar;


public class DateHelper {

    public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
        List<Date> dates = new ArrayList<Date>();
        java.util.Calendar calendar = new GregorianCalendar();
        calendar.setTime(startdate);

        while (calendar.getTime().before(enddate))
        {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(java.util.Calendar.DATE, 1);
        }
        return dates;
    }
    /*
    month should be index in range 0 to 11
     */
    public static boolean eventOverlapsMonth(java.util.Calendar eventStart, java.util.Calendar eventEnd, int year, int month) {
        java.util.Calendar monthStart = GregorianCalendar.getInstance();
        monthStart.set(java.util.Calendar.YEAR, year);
        monthStart.set(java.util.Calendar.MONTH, month);
        monthStart.set(Calendar.DAY_OF_MONTH, 0);
        monthStart.set(java.util.Calendar.HOUR_OF_DAY, 0);
        monthStart.set(java.util.Calendar.MINUTE, 0);
        monthStart.set(java.util.Calendar.SECOND, 0);
        monthStart.set(java.util.Calendar.MILLISECOND, 1);

        java.util.Calendar monthEnd = GregorianCalendar.getInstance();
        monthEnd.set(java.util.Calendar.YEAR, year);
        monthEnd.set(java.util.Calendar.MONTH, month);
        monthEnd.set(Calendar.DAY_OF_MONTH, monthEnd.getActualMaximum(java.util.Calendar.DAY_OF_MONTH));
        monthEnd.set(java.util.Calendar.HOUR_OF_DAY, 23);
        monthEnd.set(java.util.Calendar.MINUTE, 59);
        monthEnd.set(java.util.Calendar.SECOND, 59);
        monthEnd.set(java.util.Calendar.MILLISECOND, 999);

        //Log.d("eventStart", eventStart.getTime().toString());
        //Log.d("eventEnd", eventEnd.getTime().toString());
        //Log.d("monthStart", monthStart.getTime().toString());
        //Log.d("monthEnd", monthEnd.getTime().toString());

        // if start after end of month
        if (eventStart.compareTo(monthEnd) > 0) {
            return false;
        }
        // if end is before start of month
        if (eventEnd.compareTo(monthStart) < 0) {
            return false;
        }
        return true;
    }

    public static Calendar dateToCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        long tzTime = cal.getTimeInMillis() + cal.getTimeZone().getOffset(cal.getTimeInMillis());
        cal.setTimeInMillis(tzTime);
        return cal;
    }
}
