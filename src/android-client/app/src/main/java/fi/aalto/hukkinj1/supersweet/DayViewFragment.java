package fi.aalto.hukkinj1.supersweet;

import android.graphics.RectF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;


public class DayViewFragment extends CalendarFragment implements WeekView.MonthChangeListener,
        WeekView.EventClickListener, WeekView.EventLongPressListener, WeekView.EmptyViewClickListener {

    private List<Event> mEvents = new ArrayList<Event>();
    private List<WeekViewEvent> mWeekViewEvents = new ArrayList<WeekViewEvent>();

    private WeekView mDayView;
    PopupWindow mpopup;

    public static DayViewFragment newInstance() {
        DayViewFragment fragment = new DayViewFragment();
        //Bundle args = new Bundle();
        //args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        //fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void update(List<Event> events) {
        mEvents = events;
        mWeekViewEvents.clear();
        for (Event event: mEvents) {
            WeekViewEvent weekViewEvent = new WeekViewEvent(event.icaluid.hashCode(),
                    event.title, DateHelper.dateToCalendar(event.start),
                    DateHelper.dateToCalendar(event.end));
            mWeekViewEvents.add(weekViewEvent);
        }
        mDayView.notifyDatasetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_day_view, container, false);

        mDayView = (WeekView) rootView.findViewById(R.id.weekView);

        mDayView.setOnEventClickListener(this);
        mDayView.setMonthChangeListener(this);
        mDayView.setEventLongPressListener(this);
        mDayView.setEmptyViewClickListener(this);

        return rootView;
    }

    private void showEventView(final WeekViewEvent event) {
        View popUpView = getActivity().getLayoutInflater().inflate(R.layout.modify_event_view,
                null); // inflating popup layout
        mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopup.showAtLocation(popUpView, Gravity.TOP, 0, 0); // Displaying popup

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        ((EditText) popUpView.findViewById(R.id.event_title)).setText(event.getName());
        ((EditText) popUpView.findViewById(R.id.event_start_time)).setText(df.format(event.getStartTime().getTime()));
        ((EditText) popUpView.findViewById(R.id.event_end_time)).setText(df.format(event.getEndTime().getTime()));

        ((Button) popUpView.findViewById(R.id.delete_event_button)).setVisibility(View.VISIBLE);
        ((Button) popUpView.findViewById(R.id.sync_to_local_calendar_button)).setVisibility(View.VISIBLE);

        Button save_event_button = (Button) popUpView.findViewById(R.id.save_event_button);
        save_event_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CalendarActivity)getActivity()).updateEvent(event);
                mpopup.dismiss();
            }
        });

        Button delete_event_button = (Button) popUpView.findViewById(R.id.delete_event_button);
        delete_event_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CalendarActivity) getActivity()).deleteEvent(event);
                mpopup.dismiss();
            }
        });

        Button sync_to_local_calendar_button = (Button) popUpView.findViewById(R.id.sync_to_local_calendar_button);
        sync_to_local_calendar_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CalendarActivity) getActivity()).saveEventToLocalCalendar(event);
                mpopup.dismiss();
            }
        });

        Button event_cancel_button = (Button) popUpView.findViewById(R.id.cancel_event_button);
        event_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpopup.dismiss();
            }
        });
    }

    private void showNewEventView(final java.util.Calendar time) {
        View popUpView = getActivity().getLayoutInflater().inflate(R.layout.modify_event_view,
                null); // inflating popup layout
        mpopup = new PopupWindow(popUpView, LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, true); // Creation of popup
        mpopup.showAtLocation(popUpView, Gravity.TOP, 0, 0); // Displaying popup

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
        ((EditText) popUpView.findViewById(R.id.event_title)).setText("");
        ((EditText) popUpView.findViewById(R.id.event_start_time)).setText(df.format(time.getTime()));
        ((EditText) popUpView.findViewById(R.id.event_end_time)).setText(df.format(time.getTime()));

        Button save_event_button = (Button) popUpView.findViewById(R.id.save_event_button);
        save_event_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((CalendarActivity)getActivity()).createEvent(time);
                mpopup.dismiss();
            }
        });

        ((Button) popUpView.findViewById(R.id.delete_event_button)).setVisibility(View.GONE);
        ((Button) popUpView.findViewById(R.id.sync_to_local_calendar_button)).setVisibility(View.GONE);

        Button event_cancel_button = (Button) popUpView.findViewById(R.id.cancel_event_button);
        event_cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mpopup.dismiss();
            }
        });
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        showEventView(event);
    }

    @Override
    public void onEventLongPress(WeekViewEvent event, RectF eventRect) {

    }

    @Override
    public void onEmptyViewClicked(java.util.Calendar time) {
        showNewEventView(time);
    }

    @Override
    public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        return getEventsForMonth(newYear, newMonth);
    }

    private List<WeekViewEvent> getEventsForMonth(int year, int month) {
        List<WeekViewEvent> eventsForMonth = new ArrayList<WeekViewEvent>();
        for (WeekViewEvent event : mWeekViewEvents) {
            if (DateHelper.eventOverlapsMonth(event.getStartTime(), event.getEndTime(), year, month)) {
                eventsForMonth.add(event);
            }
        }
        return eventsForMonth;
    }

    public void goToDay(java.util.Calendar day) {
        mDayView.goToDate(day);
    }
}
