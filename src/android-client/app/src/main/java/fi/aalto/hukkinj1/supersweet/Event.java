package fi.aalto.hukkinj1.supersweet;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by hukkinj1 on 11/13/15.
 */
public class Event implements Serializable {
    public Date start;
    public Date end;
    public String title;
    public String icaluid;
    public String ownerId;
    public List<String> calendars;
    public String _id;

    public Event(Date start, Date end, String title) {
        this.start = start;
        this.end = end;
        this.title = title;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "Event{" +
                "start='" + start + '\'' +
                ", end='" + end + '\'' +
                ", title='" + title + '\'' +
                ", icaluid='" + icaluid + '\'' +
                ", ownerId='" + ownerId + '\'' +
                ", calendars='" + calendars + '\'' +
                ", _id='" + _id + '\'' +
                '}';
    }

    public CommunicationEvent getSendable() {
        return new CommunicationEvent(title, start, end);
    }
    public class CommunicationEvent {
        public Date start;
        public Date end;
        public String title;

        public CommunicationEvent(String title, Date start, Date end) {
            this.start = start;
            this.end = end;
            this.title = title;
        }
    }
}

