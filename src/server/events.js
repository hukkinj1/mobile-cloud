var db = require('./db');
var uuid = require('node-uuid');

var events = db.get('events');
var users = db.get('users');
var calendars = db.get('calendars');

exports.import = function(req, res) {
  if (!req.body.hasOwnProperty('icaluid')) {
    res.status(500);
    res.json({ 'error': { 'detail': 'Property "icaluid" needs to be set was not set.' }});
  }
  else {
    events.findOne({icaluid: req.body.icaluid}).on('complete', function(err, icaluidExists) {
      if (err) throw err;
      else if (icaluidExists) {
        res.status(304);  // 304 Not Modified
        res.json({ 'detail': 'An event with given iCalUID already exists. '});
      }
      else {
        exports.create(req, res);
      }
    });
  }
}

exports.create = function(req, res) {
  if (!req.user || !req.calendar) {
    throw new Error("events.create() should only be called when req.user and req.calendar exist");
  }

  if (!req.body.start || !req.body.end || !req.body.title) {
    res.status(400);  // Status: 400 Bad request
    res.json({ 'error': { 'detail': 'Some required event info was missing. Include at least start time, end time and title.' }});
    return;
  }

  if (isNaN(Date.parse(req.body.start)) || isNaN(Date.parse(req.body.end))) {
    res.status(400);  // Status: 400 Bad request
    res.json({ 'error': { 'detail': 'Could not parse ISO date.' }});
    return;
  }

  if (!req.body.hasOwnProperty('icaluid')) {
    // Create iCalUID for the event. Let's use RFC-compliant UUID. If I understood
    // correctly, this could be any UTF-8 string of up to 255 bytes.
    req.body.icaluid = uuid.v4();
  }

  // save start and end times as Date objects
  var start = new Date(req.body.start);
  var end = new Date(req.body.end);

  // Because Javascript and MongoDB gods work in mysterious ways:
  req.body.start = new Date(start.getFullYear(), start.getMonth(), start.getDate(),  start.getHours(), start.getMinutes(), start.getSeconds());
  req.body.end = new Date(end.getFullYear(), end.getMonth(), end.getDate(),  end.getHours(), end.getMinutes(), end.getSeconds());

  // make user the owner of this event
  req.body.ownerId = req.user._id;

  // If the user gave a calendars list in json body, add calendar to that list.
  if (req.body.calendars) {
    if (req.body.calendars.indexOf(req.calendar._id) < 0) {
      req.body.calendars.push(req.calendar._id);
    }
  }
  // If the user didn't send a calendars list in json body, lets create it
  else {
    req.body.calendars = [req.calendar._id];
  }

  events.insert(req.body, function(err, doc) {
    if (err) throw err;
    else {
      res.status(201); // Status: 201 Created
      res.location(req.path + doc._id);
      res.json({});
    }
  });

};

exports.update = function(req, res) {
  if (req.user && req.calendar && req.event) {
    if (req.body.start && isNaN(Date.parse(req.body.start))) {
      res.status(400);  // Status: 400 Bad request
      res.json({ 'error': { 'detail': 'Could not parse ISO date.' }});
      return;
    }
    if (req.body.end && isNaN(Date.parse(req.body.end))) {
      res.status(400);  // Status: 400 Bad request
      res.json({ 'error': { 'detail': 'Could not parse ISO date.' }});
      return;
    }

    // save start and end times as Date objects
    req.body.start = new Date(req.body.start);
    req.body.end = new Date(req.body.end);

    events.update({_id: req.event._id}, {$set: req.body}, function (err, doc) {
      if (err) {
        throw err;
      }
      else if (doc) {
        res.status(200).end();
      }
      else {
        res.status(404).json({'error': {'detail': 'Tried to update ' + req.params.event_id + ' but could not find it.'}});
      }
    })
  }
  else {
    throw new Error('Events.update should only be called when user, calendar and event exist.');
  }
};

exports.read = function(req, res) {
  res.json(req.event);
};

function parseQueryString(queryString) {
  var keyValuePairs = queryString.split('|');
  var queryFilters = {};
  keyValuePairs.forEach(function(keyValuePair) {
    var keyValueArr = keyValuePair.split('::');
    if (keyValueArr.length === 2)
      queryFilters[keyValueArr[0]] = keyValueArr[1];
  });
  return queryFilters;
};

function getQueryFilters(queryFilterString) {
  var mongoFilters = {};
  var queryFilters = parseQueryString(queryFilterString);
  if (queryFilters.start) {
    var date = new Date(queryFilters.start);
    if (date)
      mongoFilters.end = {$gt: date};
  }
  if (queryFilters.end) {
    var date = new Date(queryFilters.end);
    if (date)
      mongoFilters.start = {$lt: date};
  }
  return mongoFilters;
};

exports.readWithFilter = function(req, res) {
  var filters = {};
  if (!req.user || !req.calendar)
    throw new Error('Events.readWithFilter should only be called when user and calendar exist');
  if (req.query.filters)
    filters = getQueryFilters(req.query.filters);
  filters.calendars = {$in: [req.calendar._id]}; // Find only own events.
  events.find(filters, function(err, events) {
    if (err)
      throw err;
    else if (events) {
      res.status(200).json(events);
    }
    else {
      res.status(404).json({'error': {'detail': 'Event database not initialized'}});
    }
  });
};

exports.readAll = function(req, res) {
  var filters = {};
  if (req.query.filters) {
    filters = getQueryFilters(req.query.filters);
  }
  events.find(filters).on('complete', function (err, events) {
    if (err)
      throw err;
    else if (events) {
      res.json(events);
    }
    else {
      res.status(404).json({'error': {'detail': 'Event database not initialized'}});
    }
  })
};

exports.delete = function(req, res) {
  if (req.user && req.calendar && req.event) {
    events.remove({_id: req.event._id}).on('complete', function (err) {
      if (err)
        throw err;
      else {
        res.status(200).end();
      }
    })
  }
  else {
    throw new Error('Events.delete should only be called when user, calendar and event exist.');
  }
};