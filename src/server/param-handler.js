var db = require('./db');
var events = db.get('events');
var users = db.get('users');
var calendars = db.get('calendars');
var utils = require('./utils');


var getUser = function(req, res, next){
  users.findOne({username: req.params.user_name}).on("complete", function(err, user) {
    if (err) throw err;
    else if (user) {
      req.user = user;
      next();
    }
    else {
      res.status(404).json({"error": {"detail": "Can't find user " + req.params.user_name}});
    }
  });
};

var getCalendar = function(req, res, next){
  if (!req.user) {
    throw new Error("paramHandler.getCalendar() should only be called when req.user exists");
  }
  calendars.findOne({ownerId: req.user._id, name: req.params.calendar_name}).on("complete", function(err, calendar) {
    if (err) throw err;
    else if (calendar) {
      req.calendar = calendar;
      next();
    }
    else {
      res.status(404).json({"error": {"detail": "Can't find calendar " + req.params.calendar_name}});
    }
  });
};

var getEvent = function(req, res, next) {
  if ((req.user && !req.calendar) || (!req.user && req.calendar)) {
    throw new Error("Invalid url lead to calling paramHandler.getEvent(). Either both user and calendar should be specified in url or neither");
  }

  events.findOne({_id: req.params.event_id}).on("complete", function(err, event) {
    if (err) throw err;
    else if (event) {

      // if calendar was specified, check that the event belongs to the calendar, so that the request is valid
      if (req.calendar) {

        if (utils.isObjectInList(req.calendar._id, event.calendars)) {
          res.status(404).json({"error": {"detail": "The event " + req.params.event_id + " does not belong to the specified calendar"}});
          return;
        }
      }

      req.event = event;
      next();

    }
    else {
      res.status(404).json({"error": {"detail": "Can't find event " + req.params.event_id}});
    }
  });
};

exports.getUser = getUser;
exports.getCalendar = getCalendar;
exports.getEvent = getEvent;
