var express = require('express');
var bodyParser = require('body-parser');
var users = require('./users');
var calendars = require('./calendars');
var events = require('./events');
var db = require('./db');
var paramHandler = require('./param-handler');

var app = express();
var IP_ADDRESS = '192.168.10.102';
//var IP_ADDRESS = 'localhost';
var PORT_NUMBER = 8080;

// #############################################################################
// ###################### HTTP SERVER (EXPRESS) ################################

app.use(function(req, res, next) {
  if (req.headers['content-type'] !== undefined) {
    if (req.headers['content-type'].indexOf('application/json') === -1) {
      res.status(400).json({'error': {'detail': 'Content type should be application/json.'}});
      return;
    }
  }
  next();
});
app.use(bodyParser.json());  // for parsing application/json
app.use(function(err, req, res, next) {  // error handler for JSON parser
  console.error(err.stack);
  res.status(500).json({ 'error': { 'detail': 'Something went wrong in parsing JSON. Maybe you sent invalid JSON or too long message.' }});
});
app.use(function(req, res, next) {
  if (req.body.hasOwnProperty('_id')) {
    delete req.body['_id'];
  }
  next();
});



/*
CREATE API
Events are created using POST method, because their id is unknown to client when creating.
The PUT methods here should later be improved so that they also support complete (not partial) updates.
*/
app.put('/users/:user_name', users.create);
app.put('/users/:user_name/calendars/:calendar_name', paramHandler.getUser, calendars.create);  // create calendar with the name calendar_name
app.post('/users/:user_name/calendars/:calendar_name/events', paramHandler.getUser, paramHandler.getCalendar, events.create);  // create event and return event_id
app.post('/users/:user_name/calendars/:calendar_name/events/import', paramHandler.getUser, paramHandler.getCalendar, events.import);

/*
READ API
*/
app.get('/users/:user_name', paramHandler.getUser, users.read); //return one user
app.get('/users', users.readAll);  // return list of all users
app.get('/users/:user_name/calendars', paramHandler.getUser, calendars.readAllCalendarsByUsername); // return list of all calendars of user_name
app.get('/users/:user_name/calendars/:calendar_name', paramHandler.getUser, paramHandler.getCalendar, calendars.read);
app.get('/users/:user_name/calendars/:calendar_name/events', paramHandler.getUser, paramHandler.getCalendar, events.readWithFilter);  // return all events of the calendar. Can be filtered with query string
app.get('/users/:user_name/calendars/:calendar_name/events/:event_id', paramHandler.getUser, paramHandler.getCalendar, paramHandler.getEvent, events.read); //return the event
app.get('/events/:event_id', paramHandler.getEvent, events.read); //return the event
app.get('/events', events.readAll); //return all events Can be filtered with query string

/*
UPDATE API
Lets only use POST method here, so both partial and full updates can be done RESTfully.
*/
app.post('/users/:user_name/calendars/:calendar_name/events/:event_id', paramHandler.getUser, paramHandler.getCalendar, paramHandler.getEvent, events.update);  // update event with event_id
app.post('/users/:user_name/calendars/:calendar_name', paramHandler.getUser, paramHandler.getCalendar, calendars.update);
app.post('/users/:user_name', users.update);

/*
DELETE API
*/
app.delete('/users/:user_name/calendars/:calendar_name/events/:event_id', paramHandler.getUser, paramHandler.getCalendar, paramHandler.getEvent, events.delete);
app.delete('/users/:user_name/calendars/:calendar_name', paramHandler.getUser, paramHandler.getCalendar, calendars.delete);
app.delete('/users/:user_name', paramHandler.getUser, users.delete);

app.get('/', function(req,res) {
  var options = {
    root: __dirname
  };
  res.sendFile('public/index.html', options);
});

app.use(express.static('public'));

var httpServer = app.listen(PORT_NUMBER, IP_ADDRESS, function() {
  var host = httpServer.address().address;
  var port = httpServer.address().port;

  console.log('Express app listening at http://%s:%s', host, port);
});
