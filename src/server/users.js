var db = require('./db');
var utils = require('./utils');
var Promise = require('bluebird');

var users = db.get('users');
var events = db.get('events');
var calendars = db.get('calendars');

exports.create = function(req, res) {
  users.findOne({username: req.params.user_name}).on('complete', function(err, userExists) {
    if (err) {
      throw err;
    }
    else if (userExists) {
      // TODO: change this clause so that complete updates can be done. Or even better, make
      //       a next() call here and add some updateCompletely() function or similar to the route
      res.status(500);
      res.json({ 'error': { 'detail': 'The user already exists. This API does not yet support updates using PUT method. Use POST method instead' }});
    }
    else {
      var newUser = req.body;
      newUser.username = req.params.user_name;
      users.insert(newUser).on('complete', function (err, doc) {
        if (err) {
          throw err;
        }
        else {
          res.status(201); // Status: 201 Created
          res.end();
        }
      });
    }
  });
};

exports.update = function(req, res) {
  users.update({username: req.params.user_name}, {$set: req.body}, function(err, doc) {
    if (err) {
      if (err.name === "MongoError" && (err.code === 11001 || err.code === 11000)) {  // duplicate key error
        res.status(409).json({ 'error': { 'detail': 'New username is already taken.' }});  // 409 Conflict
      }
      else {
        throw err;
      }
    }
    else if (doc) {
      var location = req.path;
      if (req.body.username) {
        location = "/users/" + req.body.username;
      }
      res.status(200).location(location).end();
    }
    else {
      res.status(404).json({"error": {"detail": "Can't find user " + req.params.user_name}});
    }
  });

};

exports.delete = function(req, res) {
  if (req.user) {
    events.remove({ownerId: req.user._id}).on('complete', function (err) {
      if (err)
        throw err;
      calendars.remove({ownerId: req.user._id}).on('complete', function (err) {
        if (err)
          throw err;
        users.remove({_id: req.user._id}).on('complete', function (err) {
          if (err)
            throw err;
          res.status(200).end();
        });
      });
    });
  }
  else {
    throw new Error('Users.delete should only be called when the user exists.');
  }
};

exports.read = function(req, res) {
  if (req.user) {
    res.status(200).location(req.path).json(req.user);
  }
  else {
    throw new Error('Users.read should be called only if user exists.');
  }
};

exports.readAll = function(req, res) {
  users.find({}).on('complete', function(err, doc) {
    if (err) {
      throw err;
    }
    res.status(200).location(req.path).json(doc);
  })
};