var monk = require('monk');
var db = monk('localhost/test'); // Database IP and name. Change this!

var users = db.get('users');
var events = db.get('events');
var calendars = db.get('calendars');

// unique username
users.index('username', { unique: true });

// unique calendarname, ownerId combination
calendars.index('name ownerId', { unique: true });

// unique iCalUID
events.index('icaluid', { unique: true });

module.exports = db;