'use strict';
function toggleModal() {
  if ($('#modal').css('visibility') === 'visible') {
    $('#modal').css('visibility', 'hidden');
  }
  else {
    $('#modal').css('visibility', 'visible');
  }
}

function getLocalTimeZoneTimeString(time) {
  var timeWithTimezone = time.clone();
  timeWithTimezone.utcOffset(moment().utcOffset());
  return timeWithTimezone.format("YYYY-MM-DD[T]HH:mm:ss.SSSZZ");
}

function showEvent(event) {
  $('#event-title-input').val(event.title);
  $('#event-id-span').text(event.id);
  $('#event-start-time-input').val(getLocalTimeZoneTimeString(event.start));
  if (event.end === null) {
    $('#event-end-time-input').val(getLocalTimeZoneTimeString(event.start));
  }
  else {
    $('#event-end-time-input').val(getLocalTimeZoneTimeString(event.end));
  }
  $('#save-modified-event-button').show();
  $('#delete-event-button').show();
  $('#save-new-event-button').hide();
  toggleModal();
}

function showNewEvent(day) {
  $('#event-title-input').val('');
  $('#event-id-span').text('-');
  $('#event-start-time-input').val(getLocalTimeZoneTimeString(day));
  $('#event-end-time-input').val(getLocalTimeZoneTimeString(day));
  $('#save-modified-event-button').hide();
  $('#delete-event-button').hide();
  $('#save-new-event-button').show();
  toggleModal();
}

function deleteEvent(event) {
  if (!event || !event[0]) {
    console.error('No event found with clientEvents');
    return;
  }
  $.ajax({
    url: 'users/' + $('#username-input').val() + '/calendars/' + $('#calendar-name-input').val() + '/events/' + String(event[0].id),
    type: 'DELETE',
    contentType: 'application/json',
    success: function(response) {
      $('#calendar').fullCalendar('removeEvents', event[0].id);
      toggleModal();
    }
  });
}

function createEvent() {
  if (!isEventInputValid()) {
    return;
  }
  var eventJSON = formatEventJSON();
  $.ajax({
    url: 'users/' + $('#username-input').val() + '/calendars/' + $('#calendar-name-input').val() + '/events/',
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(eventJSON),
    success: function(response) {
      $('#calendar').fullCalendar('refetchEvents');
      toggleModal();
    }
  });
}

function getEvents(start, end, timezone, callback) {
  var $usernameInput = $('#username-input');
  var $calendarNameInput = $('#calendar-name-input');

  if ($usernameInput.val() === '' || $calendarNameInput.val() === '') {
    alert('Give a username and a calendar name.');
    return;
  }
  var filterString = '?filters="';
  if (start) {
    filterString += 'start::' + start.toISOString() + '|';
  }
  if (end) {
    filterString += 'end::' + end.toISOString() + '"';
  }
  $.ajax({
    url: 'users/' + $usernameInput.val() + '/calendars/' + $calendarNameInput.val() + '/events' + filterString,
    type: 'GET',
    contentType: 'application/json',
    success: function(doc) {
      var events = [];
      doc.forEach(function(event) {
        events.push({
          title: event.title,
          start: event.start,
          end: event.end,
          id: event._id
        });
      });
      callback(events);
    }
  });
}

function isEventInputValid() {
  var startDate = moment($('#event-start-time-input').val());
  var endDate = moment($('#event-end-time-input').val());
  if (!startDate.isValid() || !endDate.isValid()) {
    alert('Invalid time format! Use ISO format: YYYY-MM-DDThh:mm:ss<timezone>');
    return false;
  }
  if ($('#event-title-input').val() === '') {
    alert('You have to give a title!');
    return false;
  }
  return true;
}

function formatEventJSON() {
  var event = {};
  event.title = $('#event-title-input').val();
  event.start = moment($('#event-start-time-input').val()).utc().toISOString();
  event.end = moment($('#event-end-time-input').val()).utc().toISOString();
  return event;
}

function updateClientSideEvent(event) {
  event.title = $('#event-title-input').val();
  event.start = moment($('#event-start-time-input').val()).utc();
  event.end = moment($('#event-end-time-input').val()).utc();
}

function modifyEvent(event) {
  if (!event || !event[0]) {
    console.error('No event found with clientEvents');
    return;
  }
  if (!isEventInputValid()) {
    return;
  }
  $.ajax({
    url: 'users/' + $('#username-input').val() + '/calendars/' + $('#calendar-name-input').val() + '/events/' + $('#event-id-span').text(),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(formatEventJSON()),
    success: function(response) {
      updateClientSideEvent(event[0]);
      $('#calendar').fullCalendar('updateEvent', event[0]);
      toggleModal();
    }
  });
}