var clientId = '633199246960-fp9vse1i79gqqd7p82pojijfeqjr6ctl.apps.googleusercontent.com';
var apiKey = 'AIzaSyBg3GlsQz9n0CKzDaKu0NfkVT1GoL-9MoA';

var scopes = 'https://www.googleapis.com/auth/calendar';

// Use a button to handle authentication the first time.
function handleClientLoad() {
  gapi.client.setApiKey(apiKey);
  window.setTimeout(checkAuth,1);
}
function checkAuth() {
  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
}
function handleAuthResult(authResult) {
  var authorizeButton = document.getElementById('authorize-button');
  if (authResult && !authResult.error) {
    authorizeButton.style.visibility = 'hidden';
    loadCalendarApi();
  } else {
    authorizeButton.style.visibility = '';
    authorizeButton.onclick = handleAuthClick;
  }
}
function handleAuthClick(event) {
  gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
  return false;
}
// Load the API
function loadCalendarApi() {
  gapi.client.load('calendar', 'v3'/*, function() {
    var request = gapi.client.plus.people.get({
      'userId': 'me'
    });
    request.execute(function(resp) {
      var heading = document.createElement('h4');
      var image = document.createElement('img');
      image.src = resp.image.url;
      heading.appendChild(image);
      heading.appendChild(document.createTextNode(resp.displayName));
      document.getElementById('content').appendChild(heading);
    });
  }*/);
}


$(document).ready(function() {
  $( "#sync-to-google-button" ).click(function() {
    var username = $( '#sync-ssc-user' ).val();
    var calendarName = $( '#sync-ssc-calendar-name' ).val();
    syncToGoogle(username, calendarName);
  });

  $( "#sync-from-google-button" ).click(function() {
    var username = $( '#sync-ssc-user' ).val();
    var calendarName = $( '#sync-ssc-calendar-name' ).val();
    syncFromGoogle(username, calendarName);
  });
});

function syncFromGoogle(username, calendarName) {
  var request = gapi.client.calendar.events.list({
    'calendarId': 'primary'
  });
  request.execute(function(response) {
    var eventList = response.items;
    eventList.forEach(function(event, index) {
      // Let's only accept events that have dateTime and not just date
      if (event.start.hasOwnProperty('dateTime') && event.end.hasOwnProperty('dateTime')) {
        var sscEvent = {
          'start': event.start.dateTime,
          'end': event.end.dateTime,
          'title': event.summary,
          'icaluid': event.iCalUID
        };
        sscApiImportEvent(username, calendarName, sscEvent, function() {
          alert("Event imported from Google: " + sscEvent.title);
        });
      }
    });
  });
}




function syncToGoogle(username, calendarName) {
  sscApiGetEvents(username, calendarName, function(eventList) {
    eventList.forEach(function(event, index){

      var googleEvent = {
        'summary': event.title,
        'start': {
          'dateTime': event.start
        },
        'end': {
          'dateTime': event.end
        },
        'iCalUID': event.icaluid
      };

      var request = gapi.client.calendar.events.import({
        'calendarId': 'primary',
        'resource': googleEvent
      });
      request.execute(function(event) {
        alert('Event imported to Google Calendars: ' + event.htmlLink);
      });
    });
  });
}

function sscApiGetEvents(username, calendarName, callback) {
  $.ajax({
    url: 'users/' + username + '/calendars/' + calendarName + '/events',
    type: 'GET',
    data: JSON.stringify({}),
    contentType: 'application/json',
    dataType: 'json',
    success: function(eventList) {
      callback(eventList);
    }
  });
}

function sscApiImportEvent(username, calendarName, event, callback) {
  $.ajax({
    url: 'users/' + username + '/calendars/' + calendarName + '/events/import',
    type: 'POST',
    data: JSON.stringify(event),
    contentType: 'application/json',
    dataType: 'json',
    success: function(eventList) {
      callback();
    }
  });
}