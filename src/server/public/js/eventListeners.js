'use strict';

$.ajaxSetup({
  error: function(jqXHR, textStatus, errorThrown) {
    alert("Error: " + textStatus + ": " + errorThrown);
  }
});

$(document).ready(function() {
  $('#calendar').fullCalendar({
    header: {
      left: '',
      center: 'prev title next',
      right: 'month,agendaWeek,agendaDay'
    },

    firstDay: 1,
    defaultView: 'agendaWeek',
    timeFormat: 'H(:mm)'  ,
    timezone: 'local',

    events: getEvents,

    dayClick: function(date) {
      showNewEvent(date);
    },

    eventClick: function(event, element) {
      showEvent(event);
    }
  });

  $('#get-events-button').click(function() {
    $('#calendar').fullCalendar('refetchEvents');
  });

  $('#cancel-event-button').click(function() {
    toggleModal();
  });

  $('#delete-event-button').click(function() {
    deleteEvent($('#calendar').fullCalendar('clientEvents', $('#event-id-span').text()));
  });

  $('#save-new-event-button').click(function() {
    createEvent();
  });

  $('#save-modified-event-button').click(function() {
    modifyEvent($('#calendar').fullCalendar('clientEvents', $('#event-id-span').text()));
  });
});
