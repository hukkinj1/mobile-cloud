
var mergeAttributeBlackList = ['_id'];

exports.mergeJSON = function(source1, source2){
  var merged = Object.create(source2);
  for (var attrName in source1) {
    if (mergeAttributeBlackList.indexOf(attrName) >= 0) {
      continue;
    }
    if(merged.hasOwnProperty(attrName)) {
      if ( source1[attrName] !== null && source1[attrName].constructor == Object ) {
        merged[attrName] = mergeJSON(source1[attrName], merged[attrName]);
      }
    }
    else {
      merged[attrName] = source1[attrName];
    }
  }
  return merged;
};

exports.isObjectInList = function(object, list) {
  var i;
  for (i = 0; i < list.length; i++) {
    if (list[i] === object) {
      return true;
    }
  }
  return false;
};