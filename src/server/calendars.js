var db = require('./db');
var utils = require('./utils');
var users = db.get('users');
var calendars = db.get('calendars');


exports.create = function(req, res) {
  if (req.user) {
    calendars.findOne({name: req.params.calendar_name}).on('complete', function (err, calendar) {
      if (err) {
        throw err;
      }
      else if (calendar) {
        res.status(500).json({'error': {'detail': 'User ' + req.user.username + ' has already a calendar with that name.'}});
      }
      else {
        var newCalendar = req.body;
        newCalendar.name = req.params.calendar_name;
        newCalendar.ownerId = req.user._id;
        calendars.insert(newCalendar).on('complete', function (err, doc) {
          if (err) {
            throw err;
          }
          else {
            res.status(201).end(); // Created
          }
        });
      }
    });
  }
  else {
    throw new Error('Calendars.create should only be called when user exists.');
  }
};

exports.delete = function(req, res) {
  if (req.user && req.calendar) {
    calendars.remove({_id: req.calendar._id}).on('complete', function (err) {
      if (err)
        throw err;
      res.status(200).end();
    });
  }
  else {
    throw new Error('Calendars.delete should only be called when user and calendar exist.');
  }
};

exports.readAllCalendarsByUsername = function(req, res) {
  if (req.user) {
    calendars.find({'ownerId': req.user._id}, function(err, calendars) {
      res.status(200).location(req.path).json(calendars);
    })
  }
  else {
    throw new Error("calendars.readAllCalendarsByUsername should be called only user exists");
  }
};

exports.read = function(req, res) {
  if (req.calendar && req.user) {
    res.status(200).location(req.path).json(req.calendar);
  }
  else {
    throw new Error("calendars.read should only be called if calendar and user exist.");
  }
};

function updateCalendarById(username, changes, id, res) {
  calendars.updateById(id, changes, function(err, doc) {
    if (err) {
      throw err;
    }
    else if (doc) {
      res.status(200).location('/users/' + username + '/calendars/' + changes.name).end();
    }
    else {
      res.status(404).json({'error': {'detail': 'Tried to update ' + '/users/' + username + '/calendars/' + changes.name + ' but could not find it.'}});
    }
  });
};

exports.update = function(req, res) {
  if (req.user && req.calendar) {
    if (req.body.hasOwnProperty('name')) {
      calendars.findOne({ownerId: req.user._id, name: req.body.name}).on('complete', function (err, nameIsReserved) {
        if (err) {
          throw err;
        }
        else if (nameIsReserved) {
          res.status(500).json({'error': {'detail': 'New calendar name is already taken.'}});
        }
        else {
          updateCalendarById(req.user.username, {'$set': req.body}, req.calendar._id, res);
        }
      })
    }
    else {
      req.body.name = req.calendar.name;
      updateCalendarById(req.user.username, {'$set': req.body}, req.calendar._id, res);
    }
  }
  else {
    throw new Error('Calendars.update should only be called if user and calendar exist.');
  }
};